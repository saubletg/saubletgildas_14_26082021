# PROJECT HRnet

HRnet app. React version

## Project dependencies :
+ React v17.0.1
+ react-router-dom v5.2.0
+ Prop-types v15.7.2
+ react-icons v4.3.1

## What you need to install and run the project :

+ Git to clone the repository
+ Yarn
+ Clone the project to your computer
git clone https://gitlab.com/saubletg/saubletgildas_14_26082021.git
+ Go to the project folder
cd GildasSaublet_14_26082021
+ Install the packages
yarn
+ Run the project (port 3000 by default)
yarn start