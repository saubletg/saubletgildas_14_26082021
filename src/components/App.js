import React, { useState } from "react";
import { Switch, Route } from "react-router-dom";

import CreateEmployeePage from "./CreateEmployee/CreateEmployee";
import CurrentEmployeesPage from "./CurrentEmployees/CurrentEmployees";
import Modal from "./Modal/Modal";

const App = () => {
  const [employees, setEmployees] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);

  const createEmployee = (newEmployee) => {
    setEmployees([...employees, newEmployee]);
  };

  const handleOpenModal = () => {
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div className="app">
      {isModalOpen && (
        <Modal
          modalContent="Employee Created!"
          handleClose={handleCloseModal}
        />
      )}
      <Switch>
        <Route exact path="/home">
          <CreateEmployeePage
            handleSubmit={createEmployee}
            handleOpenModal={handleOpenModal}
          />
        </Route>
        <Route path="/employees">
          <CurrentEmployeesPage employeesList={employees} />
        </Route>
      </Switch>
    </div>
  );
};

export default App;