import React from "react";
import PropTypes from "prop-types";
import DataTables from "../../../node_modules/datatables-plugin/dist/DataTables";

import { labels } from "../../data";
import ".././DataTable/DataTable.css";

import Banner from ".././Banner/Banner";

const CurrentEmployeesPage = ({ employeesList }) => {
  const employeesData = employeesList.map((elt) => ({
    ...elt,
    department: elt.department.text,
    state: elt.state.value,
  }));

  return (
    <div className="current-employees">
      <Banner
        pageTitle="Current Employees"
        linkContent="Home"
        linkSrc="/home"
        imgSrc={`${window.location.origin}/images/logo.jpg`}
        imgAlt="logo"
      />
      <DataTables labels={labels} data={employeesData}/>
    </div>
  );
};

CurrentEmployeesPage.propTypes = {
  employeesList: PropTypes.array.isRequired,
};

export default CurrentEmployeesPage;