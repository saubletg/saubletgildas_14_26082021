import React from "react";
import PropTypes from "prop-types";

import "../Modal/Modal.css";
import { MdClose } from "react-icons/md"
import { IconContext } from "react-icons/lib";

const Modal = ({ modalContent, handleClose }) => {
  return (
    <>
      <div className="modal-overlay" onClick={handleClose}></div>
      <div className="modal">
        <span className="modal-content">{modalContent}</span>
        <button className="modal-close" type="button" onClick={handleClose}>
          <IconContext.Provider value={{ className: "modal-close-btn" }}>
            <MdClose/>
          </IconContext.Provider>
        </button>
      </div>
    </>
  );
};

Modal.propTypes = {
  modalContent: PropTypes.string.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default Modal;