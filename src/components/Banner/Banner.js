import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import "./Banner.css";

const Banner = ({ pageTitle, linkContent, linkSrc, imgSrc, imgAlt }) => {
  return (
    <div className="banner">
      <img className="logo" src= {imgSrc} alt={imgAlt}/>
      <h1>HRnet</h1>
      <Link to={linkSrc}>{linkContent}</Link>
      <h2>{pageTitle}</h2>
    </div>
  );
};

Banner.propTypes = {
  pageTitle: PropTypes.string.isRequired,
  linkContent: PropTypes.string.isRequired,
  linkSrc: PropTypes.string.isRequired,
};

export default Banner;